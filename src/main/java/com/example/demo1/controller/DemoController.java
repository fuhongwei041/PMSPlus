package com.example.demo1.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PreDestroy;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@RestController
@RequestMapping("/api/v1/demo")
public class DemoController {


    @GetMapping("hello/{input}")
    public String hello(@PathVariable("input") String input){
        return "hello " + input;
    }

    @PreDestroy
    public void shutdownDestroy() throws IOException {
        System.out.println("try to destroy...");
        File file = new File("test.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write("try to destroy...");

        bufferedWriter.close();
    }

}
